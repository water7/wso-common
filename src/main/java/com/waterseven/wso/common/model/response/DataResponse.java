package com.waterseven.wso.common.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse<T> {
//        implements Serializable {
//    private static final long serialVersionUID = -8091879091924046844L;

    private int code;

    private String message;

    private T data;
}
