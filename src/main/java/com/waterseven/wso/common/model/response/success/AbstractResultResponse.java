package com.waterseven.wso.common.model.response.success;

import com.waterseven.wso.common.model.response.AbstractResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractResultResponse<T> extends AbstractResponse {
    protected int code;
    protected String message;
    protected T data;

    public AbstractResultResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
