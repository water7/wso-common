package com.waterseven.wso.common.model.response.error;

import com.waterseven.wso.common.model.response.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AbstractErrorResponse extends AbstractResponse {
    protected int code;
    protected String message;
}
