package com.waterseven.wso.common.model.response.success;

public class BaseResultResponse<T> extends AbstractResultResponse<T> {
    public BaseResultResponse(int errorCode, T data) {
        super(errorCode, "Request was successfully!", data);
    }
}
