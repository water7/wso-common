package com.waterseven.wso.common.constant;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.logging.log4j.util.Strings;

import java.util.stream.Stream;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum EActiveStatus {
    IN_ACTIVE,
    ACTIVATION;

    public static EActiveStatus find(String name) {
        if (Strings.isBlank(name)) {
            return null;
        }
        return Stream.of(EActiveStatus.values())
                .filter(e -> name.equalsIgnoreCase(e.name())).findFirst()
                .orElse(null);
    }
}
