package com.waterseven.wso.common.constant;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import java.util.stream.Stream;

@JsonFormat(shape = JsonFormat.Shape.STRING)
@AllArgsConstructor
@Getter
public enum ERole {
    ROLE_SUPER_ADMIN("SUPER_ADMIN"),
    ROLE_ADMIN("ADMIN"),
    ROLE_LECTURER("LECTURER"),
    ROLE_STUDENT("STUDENT");

    private String text;

    /**
     * Find by name
     *
     * @param name
     * @return ERole
     */
    public static ERole find(String name) {
        if (Strings.isEmpty(name)) {
            return null;
        }
        return Stream.of(ERole.values())
                .filter(item -> item.getText().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }

}